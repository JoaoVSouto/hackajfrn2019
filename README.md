# Hackajfrn 2019

Repositório oficial do Hackajfrn 2019, cujo objetivo é fomentar a inovação e o desenvolvimento de soluções tecnológicas em prol da melhoria da prestação de serviços da Justiça para o bem-estar da sociedade.


#### REGULAMENTO

HACKATHON Justiça Federal no RN

“A Justiça de Excelência na Palma da Mão”

#### 1. Do objetivo

O evento Hackathon Justiça Federal no RN – A Justiça de Excelência na Palma da Mão,
promovido pela Campus Party e Justiça Federal no Rio Grande do Norte, tem como
objetivo fomentar a inovação e o desenvolvimento de soluções tecnológicas (na forma
de aplicações web, sites, aplicativos móveis ou equipamentos), em prol da melhoria da
prestação de serviços da Justiça para o bem-estar da sociedade.
As soluções tecnológicas deverão ser desenvolvidas observando os temas:
- Carta de serviços da JFRN;
- Melhorias de atendimento ao cidadão;
- Atendimento automático e autônomo;
- Serviços inteligentes;
- Ouvidoria e melhoria contínua de processos;

#### 2. Das datas e do local

O Hackathon Justiça Federal no RN – A Justiça de Excelência na Palma da Mão
acontecerá entre os dias 17 e 18 de Agosto de 2019 na Campus Party Natal 2 (vide
programação detalhada no item 5).
O evento fará parte da Campus Party Natal 2 entre os dias 16 e 18 de Agosto de
2019, que será realizado no Centro de Convenções Natal, em Natal - RN.

#### 3. Da inscrição

Podem efetuar a inscrição para o Hackathon pessoas físicas e com idade igual ou
superior a 16 (dezesseis) anos e que estejam devidamente registradas no evento
Campus Party Natal 2, seja por aquisição de ingresso ou cortesia. A inscrição é
individual e deve ser feita com o mesmo e-mail cadastrado no http://campuse.ro/

As inscrições se darão no período de 01 a 17 de Agosto de 2019, por meio do
preenchimento de todos os campos tidos como obrigatórios no formulário eletrônico 
de inscrição, disponibilizado no link "inscrições", no endereço:
https://hackathonjusticafederalnorn.splashthat.com/
Os inscritos preferencialmente deverão ter conhecimento e/ou experiência em, ao
menos, 01 (uma) das áreas elencadas abaixo, não é obrigatório ter habilidade em todas
as áreas:

- A) Programação e desenvolvimento de software; ou
- B) Análise de dados; o
- C) Design gráfico / digital; ou
- D) Comunicação; ou
- E) Administração ou gestão de projetos; ou
- F) Direito.

#### 4. Da participação

A participação no Hackathon é voluntária, nominativa e intransferível.
Participarão do concurso todos os interessados cujas inscrições sejam feitas no período
de 12 e 17 de Agosto de 2019, que estejam efetivamente credenciados na Campus
Party Natal 2 e compareçam no Hacka Space até às 15:30 de 17 de Agosto para
confirmar a participação.

Os participantes competirão formando equipes de até 4 (quatro) pessoas, devendo ser
observadas as habilidades indicadas acima.
No que se refere à formação das equipes:

- 1) poderão ser previamente formadas pelos próprios participantes; ou
- 2) a comissão organizadora conduzirá uma dinâmica de formação de equipes após a abertura oficial do Hackathon.

Cada equipe será composta preferencialmente por pessoas que possuam um dos
perfis citados no item 3.

Caberá ao participante levar consigo o próprio computador/laptop ou qualquer outro
material ou equipamento lícito para utilização durante todo o concurso. A Campus Party
Natal disponibilizará um ponto de rede elétrica e um ponto de rede por participante do
evento em cabo UTP ponta RJ45 (cabo de rede). É de responsabilidade do participante
levar adaptador do cabo caso seu computador/laptop não tenha a entrada para cabo 
de rede.

O participante deverá manter visível a identificação oferecida no ato do credenciamento
da Campus Party Natal 2 durante todo o período e em todos os espaços em que serão
realizadas as atividades.

#### 5. Programação do evento*
 
**17 de Agosto - Dia 1 (Sábado)**
- 14:30 Abertura no Hacka Space
- Boas-vindas
- Apresentação dos temas
- Apresentação da dinâmica do evento (Organização do Hackathon)
- 15:30 Formação de equipes
- 16:00 às 18:00 Mentorias
 
**18 de Agosto – Dia 2 (Domingo)**
- 9:00 Início da submissão dos projetos
- 12:30 Fim da submissão dos projetos
- 14:30 – 15:30 Apresentação das demos no Hacka Space
- 20:00 Cerimônia de premiação no Palco Principal
- (*) Sujeito a alterações

#### 6. Dos critérios de avaliação

No dia 18 de Agosto de 2019 (Domingo), no horário que constar na programação do
Hackathon, todas as equipes participantes apresentarão a solução tecnológica para
uma banca julgadora convidada pela comissão organizadora.

Para referida avaliação, serão utilizados os seguintes critérios:

- i) Originalidade;
- ii) Impacto;
- iii) Viabilidade técnica;
- iv) Execução.

Após a avaliação, serão consideradas vencedoras as 03 (três) equipes melhor
avaliadas pela banca julgadora.

#### 7. Da premiação

No dia 18 de Agosto de 2019 (Domingo), de acordo com a programação, será
realizada a cerimônia de premiação.
No mínimo, 01 (um) participante de cada equipe finalista deverá estar presente na
cerimônia.
Todos os prêmios oferecidos aos integrantes da equipe vencedora são pessoais e
intransferíveis.
Ao final do hackathon, de todas as equipes que submeterem suas soluções, respeitando
os horários determinados, os 3 melhores classificados na soma total de pontuação dos

critérios citados, receberão os seguintes prêmios:

**1º LUGAR**
- Ingressos Campus Party BR2020 com camping para cada membro
- Prêmio a definir

**2º LUGAR**
- Ingressos Campus Party BR2020 com camping para cada membro
- Prêmio a definir

**3º LUGAR**
- Ingressos Campus Party BR2020 sem camping para cada membro
- Prêmio a definir

#### 8. Da comunicação

Em todas as etapas do Hackathon, a comissão organizadora se comunicará com os
participantes inscritos por e-mail.
Os participantes inscritos são responsáveis por acompanharem a programação, os
resultados e eventuais alterações.
A comissão organizadora sugere a desabilitação de anti-spams que possam barrar as
comunicações sobre o Hackathon.

#### 9. Das considerações finais

O Hackathon será coordenado por uma comissão organizadora, cujas decisões são
soberanas.

Ao se inscreverem no Hackathon Justiça Federal no RN – A Justiça de Excelência na
Palma da Mão, os participantes assinalam no formulário de inscrição que concordam
com o inteiro teor do regulamento.

Os participantes, ao concordar com o Regulamento, autorizam a Justiça Federal no Rio
Grande do Norte e a Campus Party Natal a utilizar, editar, publicar, reproduzir e
divulgar, por meio de jornais, revistas, televisão, cinema, rádio e internet, vhs e cdrom, ou em qualquer outro meio de comunicação, sem ônus e sem autorização prévia
ou adicional, os seus nomes, vozes, imagens, projetos, ou empresas, tanto no âmbito
nacional quanto internacional, durante período indeterminado.

O Hackathon Hackathon Justiça Federal no RN – A Justiça de Excelência na Palma da
Mão tem o objetivo de gerar soluções para a sociedade. A Justiça Federal no Rio
Grande do Norte reserva-se o direito, sem exclusividade e mantendo o direito dos
participantes que criarem as soluções, de utilizar os projetos desenvolvidos durante o
desafio pelos participantes e equipes, para utilizá-los, reutilizá-los, reproduzi-los,
integral ou parcialmente, por todas as modalidades que julgarem adequadas, e em
quaisquer formatos, bem como imprimi-los, inseri-los ou veiculá-los em quaisquer
materiais de suporte físico ou eletrônico.

Os participantes se responsabilizam pela originalidade de todo conteúdo por eles
produzido no âmbito do presente regulamento, respondendo integral e exclusivamente
por eventuais danos ou ônus a terceiros, excluindo e indenizando a Justiça Federal no
Rio Grande do Norte e a Campus Party Natal, em caso de demanda judicial ou
extrajudicial intentada por terceiros, sob alegação de violação de direitos de
propriedade intelectual, imagem, voz e nome.

A Justiça Federal no Rio Grande do Norte e a Campus Party Natal não se
responsabilizam pelo uso de base de dados públicos e/ou privados pelos participantes
do Hackathon.

Suspeitas de conduta antiética, do não cumprimento das normas internas dos espaços
em que ocorrerá este concurso, além do desrespeito ao presente regulamento, serão
analisadas e julgadas pela comissão organizadora, podendo ainda resultar na
desclassificação do respectivo participante.
Não serão aceitas soluções tecnológicas copiadas ou reproduzidas, de forma total ou
parcial, de outras fontes e/ou competições. A identificação de uma cópia, total ou
parcial, será punida com a desclassificação da respectiva equipe.
As despesas dos participantes referentes a transporte, hospedagem, material de
consumo e quaisquer outras necessárias para a participação deste evento correrão por 
conta deles próprios.

A Justiça Federal no Rio Grande do Norte e a Campus Party Natal não se
responsabilizarão por perdas, furtos, roubos, extravios ou danos de objetos pessoais
dos participantes (como, a título exemplificativo, notebook, tablet ou celular), durante
os dias do evento. Cabe exclusivamente aos participantes o dever de guarda e cuidado
com tais pertences, assim como seguir as regras de registro de equipamentos
estabelecidos pelo evento Campus Party Natal 2. Caso o participante se ausente do
local do evento, ainda que por pouco tempo, deve levar consigo seus pertences e
equipamentos.

As decisões dos integrantes da banca julgadora no que tange à seleção e à premiação
das equipes participantes, além das decisões que a comissão organizadora venha a
dirimir, serão soberanas e irrecorríveis, não sendo cabível qualquer contestação das
mesmas, bem como dos seus resultados.
A Justiça Federal no Rio Grande do Norte e a Campus Party Natal, a seu exclusivo
critério, poderá, a qualquer tempo, se julgar necessário, alterar as regras deste
regulamento.

O concurso poderá ser interrompido ou suspenso, por motivos de força maior ou devido
a problemas de acesso à rede de internet, com servidores, entre outros, não sendo
devida qualquer indenização ou compensação aos participantes do concurso e/ou aos
eventuais terceiros. A Campus Party Natal envidará, nesses casos, os melhores esforços
para dar prosseguimento ao Hackathon tão logo haja a regularização do problema,
resguardando-se, no entanto, a possibilidade de cancelamento definitivo, na hipótese
de impossibilidade de realização do mesmo.

A participação neste concurso sujeita todos os participantes às regras e condições
estabelecidas neste regulamento. Dessa forma, os participantes, no ato de seu
cadastro/inscrição aderem a todas as disposições, declarando que leram,
compreenderam, tem total ciência e aceitam, irrestrita e totalmente, todos os itens
deste regulamento.

Os casos omissos não previstos neste regulamento serão analisados e julgados pela
comissão organizadora.